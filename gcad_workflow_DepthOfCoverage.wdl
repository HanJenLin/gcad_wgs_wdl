#####
# Author: Han-Jen Lin (hanjl@pennmedicine.upenn.edu)
# Created: May-01-2017
# Stage1  Check data-quality of mapping (pre-VCF check)
#         Sambamba depth
#####

import "stage1/depthOfCoverage.wdl" as depthOfCoverage

workflow Tutorial_gcad_steps {
  File SAMBAMBA
  File SAMTOOLS
  File input_bam
  String tmp
  String log
  String THREADS
  String RG_results
  String GCAD_LIB
  Array[File] chrs


scatter (chr in chrs) {
  call depthOfCoverage.depthOfCoverage   {

         input:
         SAMTOOLS = SAMTOOLS,
         SAMBAMBA = SAMBAMBA,
         input_bam = input_bam,
         tmp = tmp,
         log = log,
         RG_results = RG_results,
         chr = chr,
         GCAD_LIB = GCAD_LIB
  }
}

  meta {
    version: "1.0.0"
    url: "https://bitbucket.org/HanJenLin/gcad_wgs_wdl"
    SAMBAMBA: "0.6.5"
    SAMTOOLS: "1.3.1-42-g0a15035"
  }
}