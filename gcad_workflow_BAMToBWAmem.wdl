#####
# Author: Han-Jen Lin (hanjl@pennmedicine.upenn.edu)
# Created: July-31-2017
# Stage0 Roll back and Map reads to reference genome
#         Picard RevertSam
#         Picard MarkIlluminaAdapters
#####

import "stage0/streamBAM2bwa.wdl" as StreamBam2bwa


workflow Tutorial_gcad_steps {
  File BWA
  File PICARD
  File SAMTOOLS
  File input_bam
  File ref_fasta
  File ref_dict
  File ref_alt
  File ref_amb
  File ref_ann
  File ref_bwt
  File ref_pac
  File ref_sa
  File ref_fai
  String tmp
  String log
  String RG_results
  String GCAD_LIB


  call StreamBam2bwa.StreamBam2bwa  {

        input:
        BWA = BWA,
        PICARD = PICARD,
        SAMTOOLS = SAMTOOLS,
        input_bam = input_bam,
        ref_fasta = ref_fasta,
        ref_dict = ref_dict,
        ref_alt = ref_alt,
        ref_amb = ref_amb,
        ref_ann = ref_ann,
        ref_bwt = ref_bwt,
        ref_pac = ref_pac,
        ref_sa = ref_sa,
        ref_fai = ref_fai,
        tmp = tmp,
        log = log,
        RG_results = RG_results,
        GCAD_LIB = GCAD_LIB
  }


  meta {
    version: "1.0.0"
    url: "https://bitbucket.org/HanJenLin/gcad_wgs_wdl"
    SAMTOOLS: "1.3.1-42-g0a15035"
    PICARD: "2.8.1"
    BWA: "0.7.15"
  }
}
