#####
# Author: Han-Jen Lin (hanjl@pennmedicine.upenn.edu)
# Created: May-01-2017
# Stage1 Mark duplicated reads
#         Samtools sort
#         Samblaster addmatetags (CCDG version)  
#         Samtools sort
#####

import "stage1/markDup-start.wdl" as markDupstart

workflow Tutorial_gcad_steps {
  File SAMBAMBA
  File bamUtil
  File input_bam
  String tmp
  String log
  String RG_results
  String GCAD_LIB


  call markDupstart.markDupstart  {

         input:
         SAMBAMBA = SAMBAMBA,
         bamUtil = bamUtil,
         input_bam = input_bam,
         tmp = tmp,
         log = log,
         RG_results = RG_results,
         GCAD_LIB = GCAD_LIB
  }


  meta {
    version: "1.0.0"
    url: "https://bitbucket.org/HanJenLin/gcad_wgs_wdl"
    BAMUTILmem: "1.0.14"
    SAMBAMBA: "0.6.5"
    SAMTOOLS: "1.3.1-42-g0a15035"
  }
}