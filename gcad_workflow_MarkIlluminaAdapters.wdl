import "stage0/markIlluminaAdapters.wdl" as MarkIlluminaAdapters


workflow Tutorial_gcad_steps {
  File SAMTOOLS
  File PICARD
  File input_bam
  String tmp
  String GCAD_LIB
  String THREADS
  String RG_results
  
  
  call MarkIlluminaAdapters.MarkIlluminaAdapters  {

        input:
        PICARD = PICARD,
        SAMTOOLS = SAMTOOLS,
        input_bam = input_bam,
        tmp = tmp,
        THREADS = THREADS,
        RG_results = RG_results,
        GCAD_LIB = GCAD_LIB
  }


  meta {
    version: "1.0.0"
    url: "https://bitbucket.org/HanJenLin/gcad_wgs_wdl"
    SAMTOOLS: "1.3.1-42-g0a15035"
    PICARD: "2.8.1"
  }
}

