#####
# Author: Han-Jen Lin (hanjl@pennmedicine.upenn.edu)
# Created: May-01-2017
# Stage1 Mark duplicated reads
#         Samtools sort
#         Samblaster addmatetags (CCDG version)  
#         Samtools sort
#####


#sortBam_v2
task sortBam {

  File SAMTOOLS
  File SAMBLASTER
  String GCAD_LIB
  String tmp
  String log
  String RG_results
  File input_bam
  String file_basename = sub(input_bam, "\\.bam", "")

  command {
  # sorts the input file by query-name using samtools,
  # streammed to samblaster for adding in MC/MQ tags
  # output coordinate sorted BAM with MC/MQ tags
  # @param SAM - input SAM file
  # @output BAM - coordinate-sorted BAM

  
   RGRPS=`bash ${GCAD_LIB}/readgroup.sh -i ${file_basename}.bam`
   echo $RGRPS > ${file_basename}.rg.log
   
   # sort input SAM.GZ by name as required input for samblaster, output to co sorted BAM
   
   if [ $RGRPS==NULL && $RGRPS!=0 ];then
   
      RG="A"
      chrlists=`seq -f "chr%g" 1 22`
        for chr in $chrlists chrX chrY chrM other unknown;do
           
         if [ ! -s "${RG_results}/$RG.aligned.$chr.sorted.bam" ];then

          MEM="9.6G"
          if [ "$chr" == "chr1" ] || [ "$chr" == "chr2" ] || [ "$chr" == "chr3" ] || [ "$chr" == "chr4" ] || [ "$chr" == "chr5" ];then
            MEM="12G"
          fi

           qsub \
            -N sortSam-$RG-$chr \
            -cwd  \
            -o ${log}/sortbam.log \
            -l h_vmem=$MEM \
            -V \
            -hold_jid BAM2bwa-$RG \
            -j y ${GCAD_LIB}/stage1/sortBam.sh -i ${RG_results}/bam/$RG.aligned.$chr.sam.gz  -r $RG -p "${RG_results}/bam" -b "${GCAD_LIB}/stage0" -l $chr -m "${RG_results}"

         fi
        done
  else    
   
    for RG in $RGRPS;do

       chrlists=`seq -f "chr%g" 1 22`
        for chr in $chrlists chrX chrY chrM other unknown;do
           
         if [ ! -s "${RG_results}/$RG.aligned.$chr.sorted.bam" ];then

          MEM="9.6G"
          if [ "$chr" == "chr1" ] || [ "$chr" == "chr2" ] || [ "$chr" == "chr3" ] || [ "$chr" == "chr4" ] || [ "$chr" == "chr5" ];then
            MEM="12G"
          fi

           qsub \
            -N sortSam-$RG-$chr \
            -cwd  \
            -o ${log}/sortbam.log \
            -l h_vmem=$MEM \
            -V \
            -hold_jid BAM2bwa-$RG \
            -j y ${GCAD_LIB}/stage1/sortBam.sh -i ${RG_results}/bam/$RG.aligned.$chr.sam.gz  -r $RG -p "${RG_results}/bam" -b "${GCAD_LIB}/stage0" -l $chr -m "${RG_results}"

         fi
        done
     done
   fi
   
  }
  output {
  String response = read_string(stdout())
  }
}
