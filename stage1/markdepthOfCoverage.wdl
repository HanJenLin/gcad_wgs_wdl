#####
# Author: Han-Jen Lin (hanjl@pennmedicine.upenn.edu)
# Created: May-01-2017
# Stage1  markdepthOfCoverage for API
#####


task markdepthOfCoverage {


  String RG_results
  String tmp
  String log
  File input_bam
  String file_basename = sub(input_bam, "\\.bam", "")
  String GCAD_LIB

  command {

        FILENAME=`basename ${file_basename}.bam .bam`
        qsub \
            -N depth-$FILENAME-mark   \
            -cwd  \
            -o ${log}/depthOfCoverage-mark.log \
            -l h_vmem=2G \
            -V \
            -hold_jid depth-$FILENAME-chr* \
            -j y ${GCAD_LIB}/stage1/markCompletedDepthOfCoverage.sh -i ${RG_results}/bam/$FILENAME.sorted.dupmarked.bam -p "${RG_results}/bam/$FILENAME"  -b "${GCAD_LIB}/stage0"

  }
  output {
  String response = read_string(stdout())
  }
}
