#####
# Author: Han-Jen Lin (hanjl@pennmedicine.upenn.edu)
# Created: May-01-2017
# Stage1 Mark duplicated reads
#         Samtools sort
#         Samblaster addmatetags (CCDG version)  
#         Samtools sort
#####

task markDupAllBam {

  File SAMTOOLS
  File SAMBAMBA
  File bamUtil
  String GCAD_LIB
  String tmp
  String log
  String RG_results
  File input_bam
  String file_basename = sub(input_bam, "\\.bam", "")

  command {
  # sorts the input file by query-name using samtools,
  # streammed to samblaster for adding in MC/MQ tags
  # output coordinate sorted BAM with MC/MQ tags
  # @param BAM - input BAM file
  # @output BAM - coordinate-sorted BAM

  
   RGRPS=`bash ${GCAD_LIB}/readgroup.sh -i ${file_basename}.bam`
   echo $RGRPS > ${file_basename}.rg.log
   FILENAME=`basename ${file_basename}.bam .bam`
   MEM=`bash ${GCAD_LIB}/maxmem.sh -i ${file_basename}.bam`
   INPUTS=""

   if [ $RGRPS==NULL && $RGRPS!=0 ];then
      RG="A"
      chrlists=`seq -f "chr%g" 1 22`
        for chr in $chrlists chrX chrY chrM other unknown;do
          INPUTS+="${RG_results}/bam/$RG.aligned.$chr.sorted.bam "
        done
   else
     for RG in $RGRPS;do
       chrlists=`seq -f "chr%g" 1 22`
        for chr in $chrlists chrX chrY chrM other unknown;do
          INPUTS+="${RG_results}/bam/$RG.aligned.$chr.sorted.bam "
        done           
     done
   fi
   
   if [ ! -s "${RG_results}/bam/$FILENAME.sorted.dupmarked.bam" ];then
          qsub \
            -N  markDupAll-$FILENAME \
            -cwd  \
            -o ${log}/markdup.log \
            -l h_vmem=$MEM \
            -V \
            -hold_jid sortSam-*  \
            -j y ${GCAD_LIB}/stage1/markDupAllBam.sh -i "$INPUTS" -p "${RG_results}/bam/$FILENAME" -b "${GCAD_LIB}/stage0" -m "${tmp}/bam/$FILENAME"

    fi
     
  }
  output {
  String response = read_string(stdout())
  }
}
