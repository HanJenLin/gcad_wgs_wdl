import "stage0/bwamem.wdl" as BWAmem


workflow Tutorial_gcad_steps {
  File BWA
  File SAMTOOLS
  File input_bam
  File ref_fasta
  File ref_dict
  File ref_alt
  File ref_amb
  File ref_ann
  File ref_bwt
  File ref_pac
  File ref_sa
  File ref_fai
  String tmp
  String GCAD_LIB
  String RG_results
    


  call BWAmem.BWAmem  {

        input:
        BWA = BWA,
        SAMTOOLS = SAMTOOLS,
        input_bam = input_bam,
        tmp = tmp,
        GCAD_LIB = GCAD_LIB,
        RG_results = RG_results,
        ref_fasta = ref_fasta,
        ref_dict = ref_dict,
        ref_alt = ref_alt,
        ref_amb = ref_amb,
        ref_ann = ref_ann,
        ref_bwt = ref_bwt,
        ref_pac = ref_pac,
        ref_sa = ref_sa,
        ref_fai = ref_fai
  }

  meta {
    version: "1.0.0"
    url: "https://bitbucket.org/HanJenLin/gcad_wgs_wdl"
    BWA: "0.7.15"
    SAMTOOLS: "1.3.1-42-g0a15035"
  }
}

