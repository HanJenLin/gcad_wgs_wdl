import "stage0/sambambareadname_sort.wdl" as SAMBAMBAreadname_sort

workflow Tutorial_gcad_steps {
  File SAMBAMBA
  File SAMTOOLS
  File input_bam
  String tmp
  String log
  String THREADS
  String RG_results
  String GCAD_LIB

  call SAMBAMBAreadname_sort.SAMBAMBAreadname_sort   {

         input:
         SAMBAMBA = SAMBAMBA,
         SAMTOOLS = SAMTOOLS,
         input_bam = input_bam,
         tmp = tmp,
         log = log,
         THREADS = THREADS,
         RG_results = RG_results,
         GCAD_LIB = GCAD_LIB
  }


  meta {
    version: "1.0.0"
    url: "https://bitbucket.org/HanJenLin/gcad_wgs_wdl"
    SAMBAMBA: "0.6.5"
    SAMTOOLS: "1.3.1-42-g0a15035"
  }
}

