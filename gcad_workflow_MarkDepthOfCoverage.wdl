#####
# Author: Han-Jen Lin (hanjl@pennmedicine.upenn.edu)
# Created: May-01-2017
# Stage1  markdepthOfCoverage for API
#####

import "stage1/markdepthOfCoverage.wdl" as markdepthOfCoverage

workflow Tutorial_gcad_steps {

  File input_bam
  String tmp
  String log
  String RG_results
  String GCAD_LIB


  call markdepthOfCoverage.markdepthOfCoverage   {

         input:
         input_bam = input_bam,
         tmp = tmp,
         log = log,
         RG_results = RG_results,
         GCAD_LIB = GCAD_LIB
  }


  meta {
    version: "1.0.0"
    url: "https://bitbucket.org/HanJenLin/gcad_wgs_wdl"
  }
}
