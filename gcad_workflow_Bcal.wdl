#####
# Author: Han-Jen Lin (hanjl@pennmedicine.upenn.edu)
# Created: May-01-2017
# Stage2a Recalibrate reads on BAMs
#         GATK BaseRecalibrator
#####

import "stage2a/bcal.wdl" as bcal

workflow Tutorial_gcad_steps {
  File GATK
  File input_bam
  String tmp
  String log
  String THREADS
  String RG_results
  String GCAD_LIB
  File ref_fasta
  File ref_dict
  File ref_alt
  File ref_amb
  File ref_ann
  File ref_bwt
  File ref_pac
  File ref_sa
  File ref_fai
  File KNOWN
  File KNOWN_index
  File DBSNP
  File DBSNP_index
  File GOLD
  File GOLD_index


  call bcal.bcal   {

         input:
         GATK = GATK,
         input_bam = input_bam,
         tmp = tmp,
         log = log,
         THREADS = THREADS,
         ref_fasta = ref_fasta,
         ref_dict = ref_dict,
         ref_alt = ref_alt,
         ref_amb = ref_amb,
         ref_ann = ref_ann,
         ref_bwt = ref_bwt,
         ref_pac = ref_pac,
         ref_sa = ref_sa,
         ref_fai = ref_fai,
         KNOWN = KNOWN,
         KNOWN_index = KNOWN_index,
         DBSNP = DBSNP,
         DBSNP_index = DBSNP_index,
         GOLD = GOLD,
         GOLD_index = GOLD_index,
         GCAD_LIB = GCAD_LIB,
         RG_results = RG_results
  }


  meta {
    version: "1.0.0"
    url: "https://bitbucket.org/HanJenLin/gcad_wgs_wdl"
    GATK: "3.7"
  }
}

