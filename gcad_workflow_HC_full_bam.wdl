#####
# Author: Han-Jen Lin (hanjl@pennmedicine.upenn.edu)
# Created: May-01-2017
# Stage2b Performing individual genotype call, generate gVCFs (GATK) and VCFs (ATLAS) 
#         GATK HaplotypeCaller                      
#####

import "stage2b/hc_full_bam.wdl" as hc_full_bam

workflow Tutorial_gcad_steps {
  File GATK
  File input_bam
  String tmp
  String log
  String RG_results
  String GCAD_LIB
  File ref_fasta
  File ref_dict
  File ref_alt
  File ref_amb
  File ref_ann
  File ref_bwt
  File ref_pac
  File ref_sa
  File ref_fai

 
  call hc_full_bam.hc_full_bam   {

         input:
         GATK = GATK,
         input_bam = input_bam,
         tmp = tmp,
         log = log,
         ref_fasta = ref_fasta,
         ref_dict = ref_dict,
         ref_alt = ref_alt,
         ref_amb = ref_amb,
         ref_ann = ref_ann,
         ref_bwt = ref_bwt,
         ref_pac = ref_pac,
         ref_sa = ref_sa,
         ref_fai = ref_fai,
         RG_results = RG_results,
         GCAD_LIB = GCAD_LIB
  }


  meta {
    version: "1.0.0"
    url: "https://bitbucket.org/HanJenLin/gcad_wgs_wdl"
    GATK: "3.7"
  }
}


