#####
# Author: Han-Jen Lin (hanjl@upenn.edu)
# Created: May-01-2017
# Stage0 Roll back and Map reads to reference genome
#         Picard MarkIlluminaAdapters
#####


task MarkIlluminaAdapters {

  File PICARD
  File SAMTOOLS
  File input_bam
  String tmp
  String THREADS
  String RG_results
  String file_basename = sub(input_bam, "\\.bam", "")
  String GCAD_LIB

  command {

  # uses MarkIlluminaAdapters to make adapters input SAM 
  # @param SEQFILE - input SAM, single read-group
  # Output - $RG.aligned.mark.sam is a marked adapters sam file (note: per read group)  


  RGRPS=`bash ${GCAD_LIB}/readgroup.sh -i ${file_basename}.bam`

  for RG in $RGRPS;do
      if [ ! -s "${RG_results}/$RG.aligned.mark.sam" ];then

         markadapters="java -Xmx4g -jar ${PICARD}  MarkIlluminaAdapters TMP_DIR=${RG_results} METRICS=${RG_results}/$RG.metrics COMPRESSION_LEVEL=0 VALIDATION_STRINGENCY=SILENT QUIET=true INPUT=${RG_results}/$RG.aligned.sam OUTPUT=${RG_results}/$RG.aligned.mark.sam"
         echo $markadapters >  ${file_basename}.markadapters.log
         `$markadapters`
         
      fi
  done
  }
  output {
  #File aligned_sam_metrics  = "${RG_results}/$RG.metrics"
  String response = read_string(stdout())
  }
}

