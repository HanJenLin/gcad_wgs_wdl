#####
# Author: Han-Jen Lin (hanjl@upenn.edu)
# Created: May-01-2017
# Stage0 Roll back and Map reads to reference genome
#         BWA-MEM –K 100000000 and –Y (TOPMed version)
#####


task BWAmem {

  File BWA
  File SAMTOOLS
  File ref_fasta
  File ref_dict
  File ref_alt
  File ref_amb
  File ref_ann
  File ref_bwt
  File ref_pac
  File ref_sa
  File ref_fai
  String tmp
  String GCAD_LIB
  String RG_results
  File input_bam
  String file_basename = sub(input_bam, "\\.bam", "")


  command {

  # uses BWA mem to alignment FASTQ 
  # piped to BWA for mapping, output piped to split-sam-stream for
  # splitting by chr
  # @param SEQFILE - input FASTQ, single read-group, sorted by name, using original qualities
  # @param THREADS - # of threads for BWA
  # Output - $RG.aligned.chr$CHR.sam.gz is a mapped bam file (note: per read group per chromosome)  


  RGRPS=`bash ${GCAD_LIB}/readgroup.sh -i ${file_basename}.bam`

  for RG in $RGRPS;do
        
    RGLINE=`bash ${GCAD_LIB}/rgline.sh -i ${file_basename}.bam -r $RG`
    THREADS=`bash ${GCAD_LIB}/bwathreads.sh -i ${file_basename}.bam`
  
    if [ ! -s "${RG_results}/$RG.aligned" ];then

      bwamem=`${BWA} mem -t "$THREADS" -K 100000000 -Y -R "$RGLINE" -p ${ref_fasta} ${RG_results}/$RG.fastq | ${GCAD_LIB}/split-sam-stream.pl ${RG_results}/$RG.aligned`
      echo $bwamem >  ${file_basename}.bwamem.log
      `$bwamem`

     fi
  done
  }
  output {
  # File bwa_fastq  = "${file_basename}.aligned"
  String response = read_string(stdout())
  }
}
