#####
# Author: Han-Jen Lin (hanjl@upenn.edu)
# Created: May-01-2017
# Stage0 Roll back and Map reads to reference genome
#         Picard RevertSam
#####


task RevertSam {

  File PICARD
  File SAMTOOLS
  File input_bam
  #File sortedname_bam
  String tmp
  String THREADS
  String RG_results
  String file_basename = sub(input_bam, "\\.bam", "")
  String GCAD_LIB 


  command {
  
  # uses RevertSam to convert input BAM to SAM in stdout
  # @param SEQFILE - input BAM, single read-group, sorted by name, using original qualities
  # Output - $RG.aligned.sam is a readgroup sam file (note: per read group)  
   RGRPS=`bash ${GCAD_LIB}/readgroup.sh -i ${file_basename}.bam`
  
   for RG in $RGRPS;do
     if [ ! -s "${RG_results}/$RG.aligned.sam" ];then
       revertsam="java -Xmx4g -jar ${PICARD}  RevertSam TMP_DIR=${RG_results} OUTPUT_BY_READGROUP=false RESTORE_ORIGINAL_QUALITIES=true SORT_ORDER=queryname COMPRESSION_LEVEL=0 VALIDATION_STRINGENCY=SILENT QUIET=true INPUT=${RG_results}/$RG.sorted-byname.bam OUTPUT=${RG_results}/$RG.aligned.sam"
           # SANITIZE=true  need to add when test one chr
       echo $revertsam >  ${file_basename}.revertsam.log
       `$revertsam`
     fi
   done
  }

  output {
  #File aligned_sam =  "$RG.aligned.sam"
  String response = read_string(stdout())
  }
}
