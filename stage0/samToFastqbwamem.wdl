#####
# Author: Han-Jen Lin (hanjl@upenn.edu)
# Created: July-31-2017
# Stage0 Roll back and Map reads to reference genome
#         Picard SamtoFastq
#         BWA-MEM –K 100000000 and –Y (TOPMed version)
#####


task SamToFastqBWAmem {

  File PICARD
  File SAMTOOLS
  File BWA
  File ref_fasta
  File ref_dict
  File ref_alt
  File ref_amb
  File ref_ann
  File ref_bwt
  File ref_pac
  File ref_sa
  File ref_fai
  String tmp
  String THREADS
  String RG_results
  File input_bam
  String file_basename = sub(input_bam, "\\.bam", "")
  String GCAD_LIB

  command {

  # uses SamToFastq to convert input SAM to FASTQ in stdout
  # @param SEQFILE - input SAM, single read-group
  # Output - $RG.fastq is a fastq file (note: per read group)  
  # uses BWA mem to alignment FASTQ 
  # piped to BWA for mapping, output piped to split-sam-stream for
  # splitting by chr
  # @param SEQFILE - input FASTQ, single read-group, sorted by name, using original qualities
  # @param THREADS - # of threads for BWA
  # Output - $RG.aligned.chr$CHR.sam.gz is a mapped bam file (note: per read group per chromosome) 


  RGRPS=`bash ${GCAD_LIB}/readgroup.sh -i ${file_basename}.bam`
  RGLINE=`bash ${GCAD_LIB}/rgline.sh -i ${file_basename}.bam -r $RG`  
  echo " ${GCAD_LIB}/rgline.sh -i ${file_basename}.bam -r $RG ">>${file_basename}.RGLINE.log
  echo $RGLINE >>${file_basename}.RGLINE.log
  
  for RG in $RGRPS;do
    if [ ! -s "${RG_results}/$RG.fastq" ];then

      qsub \
            -N SAM2FASTQ-$RG \
            -cwd  \
            -o ${file_basename}.sam2fastq.log \
            -pe DJ ${THREADS} \
            -l h_vmem=3040M \
            -V \
            -hold_jid BAM2SAM-$RG  \
            -j y ${GCAD_LIB}/stage0/samToFastqbwamem.sh -i ${RG_results}/bam/$RG.aligned.mark.sam  -t ${THREADS} -r $RG -p "${RG_results}/bam" -b "${GCAD_LIB}/stage0" -t "${RG_results}" -l "$RGLINE"


    fi
   done

  }
  output {
  #File samto_fastq  = "${RG_results}/$RG.aligned"
  String response = read_string(stdout())
  }
}
