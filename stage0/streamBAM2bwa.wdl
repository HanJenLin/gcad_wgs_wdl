#####
# Author: Han-Jen Lin (hanjl@pennmedicine.upenn.edu)
# Created: July-31-2017
# Stage0 Roll back and Map reads to reference genome
#         Picard RevertSam
#         Picard MarkIlluminaAdapters
#####


task StreamBam2bwa {

  File PICARD
  File SAMTOOLS
  File BWA
  File ref_fasta
  File ref_dict
  File ref_alt
  File ref_amb
  File ref_ann
  File ref_bwt
  File ref_pac
  File ref_sa
  File ref_fai
  File input_bam
  String tmp
  String log
  String RG_results
  String file_basename = sub(input_bam, "\\.bam", "")
  String GCAD_LIB 

  command {
  
  # uses RevertSam to convert input BAM to SAM in stdout
  # @param SEQFILE - input BAM, single read-group, sorted by name, using original qualities
  # Output - $RG.aligned.sam is a readgroup sam file (note: per read group)  
   RGRPS=`bash ${GCAD_LIB}/readgroup.sh -i ${file_basename}.bam` 
   THREADS=`bash ${GCAD_LIB}/bwathreads.sh -i ${file_basename}.bam`
   MEM=`bash ${GCAD_LIB}/bwamem.sh -i ${file_basename}.bam` 
   echo $RGRPS > ${file_basename}.rg.log
   echo $THREADS >> ${file_basename}.bam2bwa.log
   echo $MEM >> ${file_basename}.bam2bwa.log

   if [ $RGRPS==NULL && $RGRPS!=0 ];then
   
       RG="A"
       echo "RG IS " $RG >> ${file_basename}.bam2bwa.log
       qsub \
            -N BAM2bwa-$RG \
            -cwd  \
            -o ${log}/bam2bwa.log \
            -pe DJ $THREADS \
            -l h_vmem=$MEM \
            -V \
            -hold_jid bamba-sort-flt-$RG  \
            -j y ${GCAD_LIB}/stage0/streamBAM2bwa.sh -i ${RG_results}/bam/$RG.sorted-byname.bam -r $RG -t $THREADS -p "${RG_results}/bam" -b "${GCAD_LIB}/stage0" -m "${RG_results}"

      
   else
   
     for RG in $RGRPS;do

       echo "RG IS " $RG >> ${file_basename}.bam2bwa.log
       qsub \
            -N BAM2bwa-$RG \
            -cwd  \
            -o ${log}/bam2bwa.log  \
            -pe DJ $THREADS \
            -l h_vmem=$MEM \
            -V \
            -hold_jid bamba-sort-flt-$RG  \
            -j y ${GCAD_LIB}/stage0/streamBAM2bwa.sh -i ${RG_results}/bam/$RG.sorted-byname.bam -r $RG -t $THREADS -p "${RG_results}/bam" -b "${GCAD_LIB}/stage0" -m "${RG_results}"

     done
   fi
  }

  output {
  #File aligned_sam =  "$RG.aligned.mark.sam"
  String response = read_string(stdout())
  }
}