#####
# Author: Han-Jen Lin (hanjl@upenn.edu)
# Created: July-31-2017
# Stage0 Roll back and Map reads to reference genome
#         Picard RevertSam
#         Picard MarkIlluminaAdapters
#####


task RevertSamMarkIlluminaAdapters {

  File PICARD
  File SAMTOOLS
  File input_bam
  String tmp
  String THREADS
  String RG_results
  String file_basename = sub(input_bam, "\\.bam", "")
  String GCAD_LIB 


  command {
  
  # uses RevertSam to convert input BAM to SAM in stdout
  # @param SEQFILE - input BAM, single read-group, sorted by name, using original qualities
  # Output - $RG.aligned.sam is a readgroup sam file (note: per read group)  
   RGRPS=`bash ${GCAD_LIB}/readgroup.sh -i ${file_basename}.bam`
 
   for RG in $RGRPS;do
     if [ ! -s "${RG_results}/bam/$RG.aligned.mark.sam" ];then

       qsub \
            -N BAM2SAM-$RG \
            -cwd  \
            -o ${file_basename}.bam2sam.log \
            -pe DJ ${THREADS} \
            -l h_vmem=3040M \
            -V \
            -hold_jid bamba-sort-flt-$RG  \
            -j y ${GCAD_LIB}/stage0/revertSamarkIlluminaAdapters.sh -i ${RG_results}/bam/$RG.sorted-byname.bam  -t ${THREADS} -r $RG -p "${RG_results}/bam" -b "${GCAD_LIB}/stage0" -t "${RG_results}"

     fi
   done
  }

  output {
  #File aligned_sam =  "$RG.aligned.mark.sam"
  String response = read_string(stdout())
  }
}