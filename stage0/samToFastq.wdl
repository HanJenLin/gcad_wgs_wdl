#####
# Author: Han-Jen Lin (hanjl@upenn.edu)
# Created: May-01-2017
# Stage0 Roll back and Map reads to reference genome
#         Picard SamtoFastq
#####


task SamToFastq {

  File PICARD
  File SAMTOOLS
  String tmp
  String THREADS
  String RG_results
  #File RevertSam
  File input_bam
  String file_basename = sub(input_bam, "\\.bam", "")
  String GCAD_LIB

  command {

  # uses SamToFastq to convert input SAM to FASTQ in stdout
  # @param SEQFILE - input SAM, single read-group
  # Output - $RG.fastq is a fastq file (note: per read group)  


  RGRPS=`bash ${GCAD_LIB}/readgroup.sh -i ${file_basename}.bam`
  
  for RG in $RGRPS;do
    if [ ! -s "${RG_results}/$RG.fastq" ];then
      samtofastq="java -Xmx4g -jar ${PICARD} SamToFastq CLIPPING_ATTRIBUTE=XT CLIPPING_ACTION=2 INTERLEAVE=true INCLUDE_NON_PF_READS=true INCLUDE_NON_PRIMARY_ALIGNMENTS=false TMP_DIR=${RG_results} COMPRESSION_LEVEL=0 VALIDATION_STRINGENCY=SILENT QUIET=true INPUT=${RG_results}/$RG.aligned.mark.sam FASTQ=${RG_results}/$RG.fastq"
      echo $samtofastq >  ${file_basename}.samtofastq.log
      `$samtofastq`

    fi
   done

  }
  output {
  #File samto_fastq  = "${RG_results}/$RG.fastq"
  String response = read_string(stdout())
  }
}

