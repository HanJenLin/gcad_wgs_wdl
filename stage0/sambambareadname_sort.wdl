#####
# Author: Han-Jen Lin (hanjl@upenn.edu)
# Created: May-01-2017
# Stage0 Sort bam with read names 
#         Sambamba sort
#####


task SAMBAMBAreadname_sort {

  File SAMBAMBA
  File SAMTOOLS
  String tmp
  String log
  String THREADS
  File input_bam
  String file_basename = sub(input_bam, "\\.bam", "")
  String RG_results  
  String GCAD_LIB 

  command {

         # the limit of processing -H hardware -S software -n number
         #  ulimit -Hn 65536
         #  ulimit -Sn 65536
         #  ulimit -Sn

         # sort the bam by name(queryname) while filtering out read groups into a new bam
         # @param (-i INPUTBAM)  - input BAM
         # @param (-t THREADS)   - number of thread
         # @param (-r RG)        - read-group for filtering by
         # @param (-p PREFIX)    - out-file naming prefix
         # @param (-b LIB)       - lib

         RGRPS=`bash ${GCAD_LIB}/readgroup.sh -i ${file_basename}.bam`
         echo $RGRPS > ${file_basename}.rg.log
         if [ $RGRPS==NULL && $RGRPS!=0 ];then
           RG="A"
           if [ ! -s "${RG_results}/$RG.sorted-byname.bam" ];then
               qsub \
                    -N bamba-sort-flt-$RG \
                    -cwd  \
                    -o ${log}/sambambard.log \
                    -pe DJ ${THREADS} \
                    -l h_vmem=5G \
                    -V \
                    -j y ${GCAD_LIB}/stage0/sambambareadname_sort.sh -i ${file_basename}.bam  -t ${THREADS} -r $RG -p "${RG_results}/bam" -b "${GCAD_LIB}/stage0"
  
           fi
         else
           for RG in $RGRPS;do
             if [ ! -s "${RG_results}/$RG.sorted-byname.bam" ];then
                 qsub \
                     -N bamba-sort-flt-$RG \
                     -cwd  \
                     -o ${log}/sambambard.log \
                     -pe DJ ${THREADS} \
                     -l h_vmem=5G \
                     -V \
                     -j y ${GCAD_LIB}/stage0/sambambareadname_sort.sh -i ${file_basename}.bam  -t ${THREADS} -r $RG -p "${RG_results}/bam" -b "${GCAD_LIB}/stage0"
  
              
             fi
            done
          fi
  }

  runtime {
    memory: "5G"
  }

  output {
  #File sortedname_bam  = "${file_basename}.sorted-byname.bam"
   String response = read_string(stdout())
  }
}