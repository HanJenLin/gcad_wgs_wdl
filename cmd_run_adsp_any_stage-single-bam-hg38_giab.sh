#!/bin/bash -x
echo "Begin script"
#sleep 60  #for starting cluster

export DIR="/mnt/adsp"

# Get SGE ENV
source /etc/profile.d/sge.sh

umount -v /mnt

###
# get processing code from git repo
# directory must be empty
###
rm -fR $DIR

mkdir -p $DIR

git clone -b awsImage --single-branch git@bitbucket.org:HanJenLin/gcad_wgs_wdl.git  $DIR/

export DIR="/mnt/adsp"
source $DIR/GCAD/stage0/pipeline.ini
source $DIR/GCAD/stage0/common_functions.sh
source $DIR/GCAD/stage0/seqfile_functions.sh



# Set Globals
export SM="GIAB-U1a-combined-ubam.reheader"
#export PRJ_ID=$(get_project_id)
#export FSIZE=$(get_sample_attr orig_file_size $PRJ_ID $SM)


get_instance_id && echo "\$IID:=$IID"

if [[ $(get_instance_type) != r4* ]];then
  # setup hard drives
  $DIR/GCAD/stage0/setup_mounts.sh # provide $DIR/bam/, $DIR/results/
else
  make_ebs_drive $FSIZE 8
  git clone -b awsImage --single-branch git@bitbucket.org:HanJenLin/gcad_wgs_wdl.git  $DIR/
fi

setup_dirs

cd $DIR

## grab BAM
export S3PATH="s3://wanglab-play/GIAB-U1a-combined-ubam.reheader.bam" && echo "\$S3PATH:=$S3PATH"
#export RESULTSPATH=$(get_s3_results_location $PRJ_ID $SM) && echo "\$RESULTSPATH:=$RESULTSPATH"

#if [ -z $S3PATH ] || [ -z $RESULTSPATH ];then
#  echo "Missing data paths $S3PATH $RESULTSPATH .";
#  error-to-otto "missing data paths $S3PATH $RESULTSPATH ."
#  track_item current_operation "missing-s3-download-location"
  #instant_shutdown
  #exit 1
#fi

SRT=$(date +%s)

SEQFILE=$(basename $S3PATH)

if [ ! -s "$BAM_DIR/$SEQFILE" ];then
    echo "Downloading $BAM_DIR/$SEQFILE from $S3PATH"
    echo "aws s3 cp $S3PATH $BAM_DIR/$SEQFILE"

   # track_item current_operation "s3-download"

    aws s3 cp --quiet --only-show-errors "$S3PATH" "$BAM_DIR/$SEQFILE"
    aws s3 cp "${S3PATH}.bai" "$BAM_DIR/${SEQFILE}.bai"

    if [ ! -s $BAM_DIR/$SEQFILE ];then
        #partial download, timeout
  #      error-to-otto "missing download file for $IID;$SM"
  #      track_item current_operation "s3-re-download"
        aws s3 cp --quiet --only-show-errors "$S3PATH" "$BAM_DIR/$SEQFILE"
        aws s3 cp "${S3PATH}.bai" "$BAM_DIR/${SEQFILE}.bai"


    fi
    if [ ! -s $BAM_DIR/$SEQFILE ];then
        #partial download, timeout
#        track_item current_operation "ERROR-s3-download"
#        error-to-otto "missing retry download file for $IID;$SM"
        instant_shutdown
    fi
fi

    #if [ ! -s $BAM_DIR/$SEQFILE ];then
        #partial download, timeout
    #    track_item current_operation "ERROR-s3-download"
    #    error-to-otto "missing retry download file for $IID;$SM"
    #    instant_shutdown
    #fi

    if [ "$(isCRAM $BAM_DIR/$SEQFILE)" == "1" ];then
      # track_item current_operation "CRAM2BAM"
       CRAM2BAM "$BAM_DIR/$SEQFILE" "$BAM_DIR/$SM.bam" $REF_FASTA
       rm -v "$BAM_DIR/$SEQFILE"
    else
       mv -v "$BAM_DIR/$SEQFILE" "$BAM_DIR/$SM.bam"
    fi

    if [ "$(isSorted $BAM_DIR/$SM.bam)" == "0" ];then
      ulimit -Hn 65536
      ulimit -Sn 65536
      #track_item current_operation "Sort-downloaded-file"
       $SAMBAMBA sort --tmpdir=$TMP_DIR $BAM_DIR/$SM.bam && \
          mv -v $BAM_DIR/$SM.sorted.bam $BAM_DIR/$SM.bam
       mv -v $BAM_DIR/$SM.sorted.bam.bai $BAM_DIR/$SM.bam.bai
    fi

  ZCT=$(readgroup_cnt $BAM_DIR/$SM.bam)
    if [ "$ZCT" -eq 0 ];then

       #track_item current_operation "Adding-Generic-RG"
       time add_generic_read_group $BAM_DIR/$SM.bam
    fi

ID=$SM

cp  $DIR/gcad_pipeline_hannahawsImage_A-CUHS-CU004066-BL-COL-49694BL1.json $DIR/gcad_pipeline_hannahawsImage_$ID.json
sed -i "s/ID/$ID/g" $DIR/gcad_pipeline_hannahawsImage_$ID.json

# stage0
java  -jar $DIR/cromwell-28_2.jar run $DIR/gcad_workflow_SAMBAMBAreadname_sort.wdl $DIR/gcad_pipeline_hannahawsImage_$ID.json
java  -jar $DIR/cromwell-28_2.jar run $DIR/gcad_workflow_BAMToBWAmem.wdl $DIR/gcad_pipeline_hannahawsImage_$ID.json

# stage1
java  -jar $DIR/cromwell-28_2.jar run $DIR/gcad_workflow_SortBam.wdl $DIR/gcad_pipeline_hannahawsImage_$ID.json
java  -jar $DIR/cromwell-28_2.jar run $DIR/gcad_workflow_MarkDupAllBam.wdl $DIR/gcad_pipeline_hannahawsImage_$ID.json
java  -jar $DIR/cromwell-28_2.jar run $DIR/gcad_workflow_DepthOfCoverage.wdl $DIR/gcad_pipeline_hannahawsImage_$ID.json

# stage2a
java  -jar $DIR/cromwell-28_2.jar run $DIR/gcad_workflow_Bcal.wdl $DIR/gcad_pipeline_hannahawsImage_$ID.json
java  -jar $DIR/cromwell-28_2.jar run $DIR/gcad_workflow_RTC.wdl $DIR/gcad_pipeline_hannahawsImage_$ID.json
java  -jar $DIR/cromwell-28_2.jar run $DIR/gcad_workflow_IndelRealigner.wdl $DIR/gcad_pipeline_hannahawsImage_$ID.json
java  -jar $DIR/cromwell-28_2.jar run $DIR/gcad_workflow_MergeindelRealigner.wdl $DIR/gcad_pipeline_hannahawsImage_$ID.json

# stage2b
java  -jar $DIR/cromwell-28_2.jar run $DIR/gcad_workflow_HC_full_bam.wdl $DIR/gcad_pipeline_hannahawsImage_$ID.json
java  -jar $DIR/cromwell-28_2.jar run $DIR/gcad_workflow_VariantEval.wdl $DIR/gcad_pipeline_hannahawsImage_$ID.json
