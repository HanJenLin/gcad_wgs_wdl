#!/bin/bash
#### Commandline:
# ~/gcad_workflow_stages.sh -i ~/ID.lists  -p [projectID] -s "~/cmd_run_adsp_any_stage-single-bam-hg38_V1.sh"
####


while getopts di:p:s: option
do
   case "$option" in
      d) DODEBUG=true;;       # option -d to print out commands
      i) SAMPLELIST="$OPTARG";;
      p) PROJECTID="$OPTARG";;
      s) STAGES="$OPTARG";;
   esac
done

k=0
sampleID=`(awk '{for(i=1;i<=NF;i=i+1){a[NR,i]=$i}}END{for(j=1;j<=NF;j++){str=a[1,j];for(i=2;i<=NR;i++){str=str " "  a[i,j]}print str}}' $SAMPLELIST)`
array=(${sampleID})

for k in "${array[@]}"; do

second=`(date --utc +%S)`
minute=`(date --utc +%M)`
hour=`(date --utc +%H)`
day=`(date --utc +%d)`
month=`(date --utc +%m)`
year=`(date --utc +%Y)`

aws ec2 describe-spot-price-history   --instance-types r3.8xlarge i3.8xlarge r4.8xlarge --start-time ${year}-${month}-${day}T${hour}:${minute}:00 --end-time ${year}-${month}-${day}T${hour}:${minute}:${second} --product-descriptions "Linux/UNIX (Amazon VPC)" | awk NF - > ~/price${k}.json
#aws ec2 describe-spot-price-history   --instance-types r4.8xlarge --start-time ${year}-${month}-${day}T${hour}:${minute}:00 --end-time ${year}-${month}-${day}T${hour}:${minute}:${second} --product-descriptions "Linux/UNIX (Amazon VPC)" | awk NF - > ~/price${k}.json


times=`(less ~/price${k}.json | grep "Timestamp" | wc -l)`
for x in `ls ~/price${k}.json`; do python ./parsemultinstances.py $x $times; done > ~/zones${k}.txt
zone1=`( head -n3 ~/zones${k}.txt | awk 'NR==1{print}')`
#echo $zone1
zone2=`( head -n3 ~/zones${k}.txt | awk 'NR==2{print}')`
#echo $zone2
zone3=`( head -n3 ~/zones${k}.txt | awk 'NR==3{print}')`
#echo $zone3
rm  -rf ~/zones${k}.txt
rm -rf ~/price${k}.json

if [[ $zone1 == "0" ]] ; then
   if [[ $zone2 != "0" ]] ; then
       zone=$zone2
       #echo "zone2"$zone
   else
       zone=$zone3
       #echo "zone3"$zone
   fi
else
   zone=$zone1
   #echo "zone1"$zone
fi


 echo "ID: " $k
 Instance=`(echo $zone | awk '{ print $1 }')`
 Z=`(echo $zone | awk '{ print $2 }')`
 P=`(echo $zone | awk '{ print $3 }')`

if [[ ${P%.*} == "0" ]] ; then
 if [ $Instance == "i3.8xlarge" ] ; then
    echo /usr/local/bin/starcluster  start -c adni-vpc-wgs-bid-big-${Z}  -U $STAGES  -P wanglab-P${PROJECTID}-${k} -I i3.8xlarge
    /usr/local/bin/starcluster  start -c adni-vpc-wgs-bid-big-${Z}  -U $STAGES  -P wanglab-P${PROJECTID}-${k} -I i3.8xlarge
 elif [ $Instance == "r4.8xlarge" ] ; then
    echo /usr/local/bin/starcluster  start -c adni-vpc-wgs-bid-big-${Z}  -U $STAGES  -P wanglab-P${PROJECTID}-${k} -I r4.8xlarge
    /usr/local/bin/starcluster  start -c adni-vpc-wgs-bid-big-${Z}  -U $STAGES  -P wanglab-P${PROJECTID}-${k} -I r4.8xlarge
 else
    echo /usr/local/bin/starcluster  start -c adni-vpc-wgs-bid-big-${Z}  -U $STAGES  -P wanglab-P${PROJECTID}-${k}
    /usr/local/bin/starcluster  start -c adni-vpc-wgs-bid-big-${Z}  -U $STAGES  -P wanglab-P${PROJECTID}-${k}
 fi
else
   echo "Wait for cheaper price~"
   echo "ID: " $k >> ~/rerun.lists
   sleep 60
fi
   #/usr/local/bin/starcluster sshmaster wanglab-P${PROJECTID}-${k}

done

