#####
# Author: Han-Jen Lin (hanjl@pennmedicine.upenn.edu)
# Created: May-01-2017
# Stage2a Recalibrate reads on BAMs
#         GATK IndelRealigner
#####


task indelRealigner {

  File GATK
  String tmp
  String log
  String THREADS
  String RG_results
  String GCAD_LIB
  File input_bam
  File ref_fasta
  File ref_dict
  File ref_alt
  File ref_amb
  File ref_ann
  File ref_bwt
  File ref_pac
  File ref_sa
  File ref_fai
  File KNOWN
  File KNOWN_index
  File DBSNP
  File DBSNP_index
  File GOLD
  File GOLD_index
  String file_basename = sub(input_bam, "\\.bam", "")

  command {
    
    MEM="17G"
    REGIONS="${file_basename}.indels.intervals"
    BQSR="${file_basename}.recal_data.table"
    chrlists=`seq -f "chr%g" 1 22`
    for CHR in $chrlists chrX chrY chrM other unmapped;do

       if [ $CHR = "other" ];then
           MEM="17.5G"
           L=`seq -f"-XL chr%.0f" -s' ' 1 22`
           L+=" -XL chrX -XL chrY -XL chrM -XL unmapped"
       else
           L="-L $CHR"
       fi

     FILENAME=`basename ${file_basename}.bam .bam`

      if [ ! -s "${RG_results}/bam/$FILENAME.indelrealign.$CHR.bam" ];then
        qsub \
            -N IndelRealign-$FILENAME-$CHR \
            -cwd  \
            -o ${log}/indelrealign.$CHR.log \
            -l h_vmem=$MEM \
            -V \
            -hold_jid RTC-$FILENAME \
            -j y ${GCAD_LIB}/stage2a/indelRealigner.sh -i "${RG_results}/bam/$FILENAME.sorted.dupmarked.bam" -b "${GCAD_LIB}/stage0" -p "${RG_results}/bam/$FILENAME"  -m "${RG_results}" -l "$L" -c "$CHR"

                    
      fi
    done
  }

  output {
   String response = read_string(stdout())
  }
}