#####
# Author: Han-Jen Lin (hanjl@upenn.edu)
# Created: May-01-2017
#Stage2a Recalibrate reads on $READGROUPS_BAMs
#         GATK RealignTargetCreator
#####


task rtc {

  File GATK
  String tmp
  String THREADS
  String RG_results
  String GCAD_LIB
  File input_bam
  File ref_fasta
  File ref_dict
  File ref_alt
  File ref_amb
  File ref_ann
  File ref_bwt
  File ref_pac
  File ref_sa
  File ref_fai
  File KNOWN
  File KNOWN_index
  File DBSNP
  File DBSNP_index
  File GOLD
  File GOLD_index
  String file_basename = sub(input_bam, "\\.bam", "")


  command {

    FILENAME=`basename ${file_basename}.bam .bam`
    MEM=`bash ${GCAD_LIB}/maxmem.sh -i ${file_basename}.bam`
    THREADS=`bash ${GCAD_LIB}/bwathreads.sh -i ${file_basename}.bam`
    RGRPS=`bash ${GCAD_LIB}/readgroup.sh -i ${file_basename}.bam`
    chrlists=`seq -f "chr%g" 1 22`

   for RG in $RGRPS;do
     if [ ! -s "${RG_results}/bam/$RG.aligned.$chr.indels.intervals" ];then
       for chr in $chrlists chrX chrY chrM other unknown;do
        qsub \
            -N RTC-$RG-$chr  \
            -cwd  \
            -o ${RG_results}/bam/RTC-$RG-$chr-rtc.log \
            -l h_vmem=4G \
            -V \
            -hold_jid sortSam-$RG-$chr \
            -j y ${GCAD_LIB}/stage2a/rtc.sh -i "${RG_results}/bam/$RG.aligned.$chr.sorted.bam" -b "${GCAD_LIB}/stage0" -p "${RG_results}/bam/$RG.aligned.$chr" -t $THREADS -m "${RG_results}"
       done
     

     fi
   done
  }

  output {
  #File rtc_intervals  = "${file_basename}.indels.intervals"
   String response = read_string(stdout())
  }
}
