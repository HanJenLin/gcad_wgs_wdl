#####
# Author: Han-Jen Lin (hanjl@pennmedicine.upenn.edu)
# Created: May-01-2017
# Stage1 Mark duplicated reads
#         Samtools sort
#         Samblaster addmatetags (CCDG version)  
#         Samtools sort
#####

import "stage1/sortBam.wdl" as sortBam

workflow Tutorial_gcad_steps {
  File SAMBLASTER
  File SAMTOOLS
  File input_bam
  String GCAD_LIB
  String tmp
  String log
  String RG_results


  call sortBam.sortBam   {

         input:
         SAMTOOLS = SAMTOOLS, 
         SAMBLASTER = SAMBLASTER,
         input_bam = input_bam,
         GCAD_LIB = GCAD_LIB,
         tmp = tmp,
         log = log,
         RG_results = RG_results

  }

  meta {
    version: "1.0.0"
    url: "https://bitbucket.org/HanJenLin/gcad_wgs_wdl"
    SAMBLASTER: "0.1.24"
    SAMTOOLS: "1.3.1-42-g0a15035"
  }
}