#!/bin/bash


while getopts di:r: option
do
   case "$option" in
      d) DODEBUG=true;;       # option -d to print out commands
      i) SAMPLE="$OPTARG";;
      r) RG="$OPTARG";;
   esac
done

 NM=$RG
 RGLINE=$(samtools view -H  $SAMPLE | grep  "^@RG" | grep -m 1 -P "ID:${NM}\s" |sed 's/\t/\\t/g')

# echo "samtools view -H  "${SAMPLE}" | grep  "^@RG" | grep -m 1 -P "ID:${NM}\s" |sed 's/\t/\\t/g'"
 echo "${RGLINE}"
