#!/bin/bash
#
# samToFastqbwamem.sh
#
# @param (-i INPUTSAM)  - input SAM
# @param (-t THREADS)   - number of thread
# @param (-r RG)        - read-group for filtering by
# @param (-p PREFIX)    - out-file naming prefix
# @param (-b LIB)       - lib
# @param (-t TMP)       - tmp
# @param (-l RGLINE)    - rgline

while getopts dnt:i:p:r:b:t:l: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      t) THREADS="$OPTARG";;
      i) INPUTSAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      r) RG="$OPTARG";;
      b) LIB="$OPTARG";;
      t) TMP="$OPTARG";;
      l) RGLINE="$OPTARG";;
   esac
done


source ${LIB}/pipeline.ini #REF_FASTA, GATK, DBSNP, LOGS from ENV
source ${LIB}/common_functions.sh

 java -Xmx4g -jar $PICARD SamToFastq \
           CLIPPING_ATTRIBUTE=XT \
           CLIPPING_ACTION=2 \
           INTERLEAVE=true \
           INCLUDE_NON_PF_READS=true \
           INCLUDE_NON_PRIMARY_ALIGNMENTS=false \
           TMP_DIR=${TMP} \
           COMPRESSION_LEVEL=0 \
           VALIDATION_STRINGENCY=SILENT \
           QUIET=true \
           INPUT=${INPUTSAM} \
           FASTQ=/dev/stdout | \
             $BWA mem \
                    -t "${THREADS}" \
                    -K 100000000 \
                    -Y \
                    -p $REF_FASTA - | \
                    ${LIB}/split-sam-stream.pl ${PREFIX}/${RG}.aligned