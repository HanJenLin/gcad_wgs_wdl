#!/bin/bash
#
# revertSamarkIlluminaAdapters.sh
#
# @param (-i INPUTBAM)  - input BAM
# @param (-t THREADS)   - number of thread
# @param (-r RG)        - read-group for filtering by
# @param (-p PREFIX)    - out-file naming prefix
# @param (-b LIB)       - lib
# @param (-t TMP)       - tmp

while getopts dnt:i:p:r:b:t: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      t) THREADS="$OPTARG";;
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      r) RG="$OPTARG";;
      b) LIB="$OPTARG";;
      t) TMP="$OPTARG";;
   esac
done


source ${LIB}/pipeline.ini #REF_FASTA, GATK, DBSNP, LOGS from ENV
source ${LIB}/common_functions.sh



    java -Xmx500M -jar $PICARD  RevertSam \
            TMP_DIR=${TMP} \
            OUTPUT_BY_READGROUP=false \
            RESTORE_ORIGINAL_QUALITIES=true \
            SORT_ORDER=queryname \
            COMPRESSION_LEVEL=0 \
            VALIDATION_STRINGENCY=SILENT \
            QUIET=true \
            INPUT=${INPUTBAM} \
            OUTPUT=/dev/stdout | \
              java -Xmx500M -jar $PICARD  MarkIlluminaAdapters \
                   TMP_DIR=${TMP} \
                   METRICS=${PREFIX}/${RG}.metrics \
                   COMPRESSION_LEVEL=0 \
                   VALIDATION_STRINGENCY=SILENT \
                   QUIET=true \
                   INPUT=/dev/stdin \
                   OUTPUT=${PREFIX}/${RG}.aligned.mark.sam