#!/bin/bash
#####
# Author: Han-Jen Lin (hanjl@pennmedicine.upenn.edu)
# Created: May-01-2017
#Stage2a Recalibrate reads on BAMs
#         GATK BaseRecalibrator
#####

# bcal.sh
#
# @param (-i INPUTBAM)  - input BAM
# @param (-r RG)        - read-group for filtering by
# @param (-p PREFIX)    - out-file naming prefix
# @param (-b LIB)       - lib
# @param (-m TMP)       - tmp
# @param (-t THREADS)   - threads

while getopts dn:i:p:r:b:m:t: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      r) RG="$OPTARG";;
      b) LIB="$OPTARG";;
      m) TMP="$OPTARG";;
      t) THREADS="$OPTARG";;
   esac
done


source ${LIB}/pipeline.ini #REF_FASTA, GATK, DBSNP, LOGS from ENV
source ${LIB}/common_functions.sh
export SM=$(get_sample_name)
export PRJ_ID=$(get_project_id)

SRT=$(date +%s)
NOW=$(date '+%F+%T')
track_item bcal_table_start_tm $NOW
track_item current_operation "BaseRecalibrator"

      java -Djava.io.tmpdir=${TMP} \
           -jar $GATK -T BaseRecalibrator \
           -R $REF_FASTA \
           -I ${INPUTBAM} \
           -nct ${THREADS} \
           --useOriginalQualities \
           -knownSites $KNOWN \
           -knownSites $DBSNP \
           -knownSites $GOLD \
           -o ${PREFIX}.recal_data.table

if [ ! -s "${PREFIX}.recal_data.table" ];then
  error-to-hannah "BaseRecalibrator-ErrorState on $IID;$SM"
  track_item current_operation "BaseRecalibrator-ErrorState"
else
  END=$(date +%s)
  TM=$[$END-$SRT];
  NOW=$(date '+%F+%T')
  track_item bcal_duration $TM
  track_item bcal_done_time $NOW
  
   if [ -z "$RESULTSPATH" ];then
     export RESULTSPATH=$(get_s3_results_location $PRJ_ID $SM)
   fi
   if [ ! -z "$RESULTSPATH" ];then
      TBL=$(basename ${PREFIX}.recal_data.table)

      aws s3 cp --storage-class STANDARD_IA ${PREFIX}.recal_data.table $RESULTSPATH/$TBL
      track_item gatk_bqsr_recal_data_table_path $(rawurlencode "$RESULTSPATH/$TBL")
   fi 
fi

BLOGFILE="$LOGS/recal.log"

track_item current_operation "MarkCompleteBCAL"

if [ -s "$BLOGFILE" ];then
  READS=$(grep 'total reads' $BLOGFILE | awk '{print $5}')
  track_item gatk_br_processed_reads $READS

  RECALREADS=$(grep 'total reads' $BLOGFILE | awk '{print $16}')
  track_item gatk_br_recalibrated_reads $RECALREADS

  FILTERPERC=$(grep 'total reads' $BLOGFILE | grep -Po "\d+\.\d\d")
  track_item gatk_br_total_filter_perc $FILTERPERC

  CIGARFILTER=$(grep 'BadCigarFilter' $BLOGFILE | grep -Po "\d+\.\d\d")
  track_item gatk_br_BadCigarFilter_perc $CIGARFILTER

  DUPLICATEFILTER=$(grep 'DuplicateReadFilter' $BLOGFILE | grep -Po "\d+\.\d\d")
  track_item gatk_br_DuplicateReadFilter_perc $DUPLICATEFILTER

  MAPFILTER=$(grep 'MappingQualityZeroFilter' $BLOGFILE | grep -Po "\d+\.\d\d")
  track_item gatk_br_MappingQualityZeroFilter_perc $MAPFILTER

  ALIGNFILTER=$(grep 'NotPrimaryAlignmentFilter' $BLOGFILE | grep -Po "\d+\.\d\d");
  track_item gatk_br_NotPrimaryAlignmentFilter_perc $ALIGNFILTER

fi



