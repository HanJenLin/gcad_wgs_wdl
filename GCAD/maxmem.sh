#!/bin/bash

export DIR="/mnt/adsp"
source $DIR/GCAD/stage0/pipeline.ini
source $DIR/GCAD/stage0/common_functions.sh
source $DIR/GCAD/stage0/seqfile_functions.sh

while getopts di: option
do
   case "$option" in
      d) DODEBUG=true;;       # option -d to print out commands
      i) SAMPLE="$OPTARG";;
   esac
done

 MEM=$(get_max_mem)
 echo "${MEM}"