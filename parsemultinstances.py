#####
# Author: Han-Jen Lin (hanjl@pennmedicine.upenn.edu)
# Created: July-31-2017
# Choose Spot Instance Type, Price, Zone
#####

import json
import sys
from pprint import pprint

jdata = open(sys.argv[1])

data = json.load(jdata)

#print "instanceType", " - ", "AvailabilityZone", " - ", "SpotPrice"
instance_tuples=[]
price_tuples=[]
zone_tuples=[]
#print 'Timestampsnum:', str(sys.argv[2])
for index in range(int(sys.argv[2])):
    instance=data["SpotPriceHistory"][index]["InstanceType"]
    price=data["SpotPriceHistory"][index]["SpotPrice"]
    zone=data["SpotPriceHistory"][index]["AvailabilityZone"]
#    print instance, price, zone
    instance_tuples.append(instance)
    price_tuples.append(price)
    zone_tuples.append(zone)
price_zone=dict(zip(zip(zone_tuples, price_tuples), instance_tuples))
pzsort=sorted(price_zone.items(), key=lambda value: value[0][1])
#for num in range(len(pzsort)):
#    print pzsort[num][0]  #(zone, price)
#    print pzsort[num][0][1] #price
#    print pzsort[num][1] #instanceType
#print pzsort
for num in range(len(pzsort)):
    if   pzsort[num][0][0] == "us-east-1a":
         print pzsort[num][1], ' a', pzsort[num][0][1]
    elif pzsort[num][0][0] == "us-east-1b":
         print pzsort[num][1], ' b', pzsort[num][0][1]
    elif pzsort[num][0][0] == "us-east-1d":
         print pzsort[num][1], ' d', pzsort[num][0][1]
    elif pzsort[num][0][0] == "us-east-1e":
         print pzsort[num][1], ' e', pzsort[num][0][1]
    else:
         print("0")

jdata.close()
