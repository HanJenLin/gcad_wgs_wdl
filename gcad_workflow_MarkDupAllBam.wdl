#####
# Author: Han-Jen Lin (hanjl@pennmedicine.upenn.edu)
# Created: May-01-2017
# Stage1 Mark duplicated reads
#         Samtools sort
#         Samblaster addmatetags (CCDG version)  
#         Samtools sort
#####

import "stage1/markDupAllBam.wdl" as markDupAllBam

workflow Tutorial_gcad_steps {
  File SAMBAMBA
  File SAMTOOLS
  File bamUtil
  File input_bam
  String tmp
  String log
  String RG_results
  String GCAD_LIB


  call markDupAllBam.markDupAllBam  {

         input:
         SAMTOOLS = SAMTOOLS,
         SAMBAMBA = SAMBAMBA,
         bamUtil = bamUtil,
         input_bam = input_bam,
         tmp = tmp,
         log = log,
         RG_results = RG_results,
         GCAD_LIB = GCAD_LIB
  }


  meta {
    version: "1.0.0"
    url: "https://bitbucket.org/HanJenLin/gcad_wgs_wdl"
    BAMUTILmem: "1.0.14"
    SAMBAMBA: "0.6.5"
    SAMTOOLS: "1.3.1-42-g0a15035"
  }
}
