#####
# Author: Han-Jen Lin (hanjl@pennmedicine.upenn.edu)
# Created: May-01-2017
# Stage2b Performing individual genotype call, generate gVCFs (GATK) and VCFs (ATLAS)
#         GATK VariantEval
#####

import "stage2b/variantEval.wdl" as variantEval

workflow Tutorial_gcad_steps {
  File GATK
  File input_bam
  String tmp
  String log
  String THREADS
  String RG_results
  String GCAD_LIB
  File ref_fasta
  File ref_dict
  File ref_alt
  File ref_amb
  File ref_ann
  File ref_bwt
  File ref_pac
  File ref_sa
  File ref_fai
  File DBSNP
  File DBSNP_index
  File GOLD
  File GOLD_index


  call variantEval.variantEval   {

         input:
         GATK = GATK,
         input_bam = input_bam,
         tmp = tmp,
         log = log,
         THREADS = THREADS,
         ref_fasta = ref_fasta,
         ref_dict = ref_dict,
         ref_alt = ref_alt,
         ref_amb = ref_amb,
         ref_ann = ref_ann,
         ref_bwt = ref_bwt,
         ref_pac = ref_pac,
         ref_sa = ref_sa,
         ref_fai = ref_fai,
         DBSNP = DBSNP,
         DBSNP_index = DBSNP_index,
         GOLD = GOLD,
         GOLD_index = GOLD_index,
         RG_results = RG_results,
         GCAD_LIB = GCAD_LIB
  }



  meta {
    version: "1.0.0"
    url: "https://bitbucket.org/HanJenLin/gcad_wgs_wdl"
    GATK: "3.7"
  }
}
