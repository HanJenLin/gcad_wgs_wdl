#####
# Author: Han-Jen Lin (hanjl@pennmedicine.upenn.edu)
# Created: July-31-2017
# Choose Spot Instance Zone
#####

import json
import sys
from pprint import pprint

jdata = open(sys.argv[1])

data = json.load(jdata)

#print "AvailabilityZone", " - ", "SpotPrice"
price_tuples=[]
zone_tuples=[]
#print 'Timestampsnum:', str(sys.argv[2])
for index in range(int(sys.argv[2])):
    price=data["SpotPriceHistory"][index]["SpotPrice"]
    zone=data["SpotPriceHistory"][index]["AvailabilityZone"]
    #print price, zone
    price_tuples.append(price)
    zone_tuples.append(zone)
price_zone=dict(zip(zone_tuples, price_tuples))
pzsort=sorted(price_zone.items(), key=lambda value: value[1])
for num in range(len(pzsort)):
    if   pzsort[num][0] == "us-east-1a":
         print("a")
    elif pzsort[num][0] == "us-east-1b":
         print("b")
    elif pzsort[num][0] == "us-east-1d":
         print("d")
    #elif pzsort[num][0] == "us-east-1e":
    #     print("e")
    else:
         print("0")

jdata.close()
